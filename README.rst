pcapview
========

Convert PCAP to JSON for embedding interactive pcap in a webpage

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Dump one line JSON list of pcapfile::

    $ pcapview -j mypcap.pcap

Dump indented 4 whitespace JSON list of pcapfile::

    $ pcapview -j mypcap.pcap -i 4

Dump to file::

    $ pcapview -j mypcap.pcap -o mypcap.json

Use --help/-h to view info on the arguments::

    $ pcapview --help

Release Notes
-------------

:0.0.3:
    - process type 1 (A) and type 5 (CNAME) DNS queries and rewrite IP source, dest
    - Add -r/--rel-time relative time option (output as relative time to first packet)
    - Add "info" key for summary of packet
:0.0.2:
    - Add -o/--output option
:0.0.1:
    - Project created
