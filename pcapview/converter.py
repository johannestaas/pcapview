#!/usr/bin/env python
import sys
import json
from datetime import datetime
from binascii import hexlify
from scapy.all import rdpcap


def debytify(val):
    if not isinstance(val, bytes):
        return val
    try:
        return val.decode('utf8')
    except UnicodeDecodeError:
        return hexlify(val).decode('utf8')


def fix_byte_fields(fields):
    if fields is None:
        return None
    data = {}
    if hasattr(fields, 'fields'):
        fields = fields.fields
    if isinstance(fields, dict):
        fields = fields.items()
    for k, v in fields:
        data[k] = debytify(v)
    return data


def convert_packet(pkt, reltime=False):
    data = {}
    payload = pkt
    data['timestamp'] = datetime.utcfromtimestamp(payload.time).strftime(
        '%Y-%m-%dT%H:%M:%S.%f')
    if reltime:
        data['time'] = payload.time - reltime
    else:
        data['time'] = payload.time
    order = []
    while payload:
        name = payload.name
        inner = {}
        for key, val in payload.fields.items():
            if key in ('load', 'padding'):
                continue
            val = debytify(val)
            key2 = '{}_{}'.format(name, key)
            if key2 in ('DNS_qd', 'DNS_an', 'TCP_options'):
                inner[key] = fix_byte_fields(val)
            else:
                try:
                    json.dumps(val)
                    inner[key] = val
                except TypeError:
                    print('Failed to dump {}'.format(key2), file=sys.stderr)
                    print('value: {!r}'.format(val), file=sys.stderr)
        if inner:
            order.append(name)
            data[name] = inner
        payload = payload.payload
    data['layers'] = order
    data['info'] = pkt.summary()
    return data


def convert_pcap(path, reltime=False):
    pcap = rdpcap(path)
    if reltime and pcap:
        reltime = pcap[0].time
    parsed = [convert_packet(p, reltime=reltime) for p in pcap]
    return parsed
