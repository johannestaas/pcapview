'''
pcapview

Convert PCAP to JSON for embedding interactive pcap in a webpage
'''

__title__ = 'pcapview'
__version__ = '0.0.2'
__all__ = ()
__author__ = 'Johan Nestaas <johannestaas@gmail.com>'
__license__ = 'GPLv3+'
__copyright__ = 'Copyright 2016 Johan Nestaas'


from .converter import convert_pcap


def main():
    import json
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('--json', '-j', action='store_true',
                        help='convert pcap file to json')
    parser.add_argument('--indent', '-i', type=int, default=None,
                        help='indentation on json output')
    parser.add_argument('--rel-time', '-r', action='store_true',
                        help='output time as relative to first packet')
    parser.add_argument('--output', '-o', help='output to path')
    parser.add_argument('path')
    args = parser.parse_args()
    data = convert_pcap(args.path, reltime=args.rel_time)
    if args.json:
        if args.indent is None:
            s = json.dumps(data)
        else:
            s = json.dumps(data, indent=args.indent)
        if args.output:
            with open(args.output, 'w') as f:
                f.write(s)
        else:
            print(s)
    else:
        parser.print_usage()


if __name__ == '__main__':
    main()
